var col = 0;
var row = 0;

window.onload = function () {
	colorise(row, col, "red");
}

document.onkeydown = function(event) {
	var keycod = event.keyCode;
	var smjer;
	if (keycod == 37) {
		smjer = "left";
	} else if (keycod == 38) {
		smjer = "up";
	} else if (keycod == 39) {
		smjer = "right";
	} else if (keycod == 40) {
		smjer = "down";
	}

	if (smjer) {
		move(smjer);
	}
}



function move(smjer) {
	var newCol;
	var newRow;
	if (smjer == 'up') {
		newRow = row - 1;
		newCol = col;
	} else if (smjer == 'down') {
		newRow = row + 1;
		newCol = col;
	} else if (smjer == 'right') {
		newRow = row;
		newCol = col + 1;
	} else if (smjer == 'left') {
		newRow = row;
		newCol = col - 1;
	}

	newRow = fixFallout(newRow);
	newCol = fixFallout(newCol);

	colorise(row, col, "white");
	colorise(newRow, newCol, "red");
	row = newRow;
	col = newCol;
}

function fixFallout(index) {
	if (index < 0) {
		index = 0;
	} else if (index > 3) {
		index = 3;
	}
	return index;
}

function colorise(cRow, cCol, cColor) {
	var cId = 'c' + cRow + cCol;
	var cell = document.getElementById(cId);
	cell.style = "background: " + cColor;
}